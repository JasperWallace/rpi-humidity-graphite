#!/usr/bin/python

import time, sys, threading, argparse
from datetime import datetime
from graphiteclient import GraphiteClient

from HIH6130.io import HIH6130

# https://www.sparkfun.com/products/11295
# https://www.davidhagan.me/articles?id=18

server = port = None

parser = argparse.ArgumentParser(description='send temp and humidity stats to graphite.')

parser.add_argument('--server', type=str,
  help='The graphite server hostname')

parser.add_argument('--port',
  type=int,
  help='The graphite server port (defaults to 2003)')

parser.add_argument('--bus',
  type=int,
  help='The I2C bus to use, defaults to 1')

args = parser.parse_args()

if args.server:
  server = args.server

if args.port:
  port = args.port

if server:
  client = GraphiteClient(server, port)
else:
  client = GraphiteClient()

bus = 1
# can't just check true/false here since bus==0 is a valid bus number
if args.bus is not None:
  bus = args.bus

# XXX default address
rht = HIH6130(bus=bus)

while True:
  rht.read()
  client.poke("environment.temperature.dc-HIH6130.%s", rht.t)
  client.poke("environment.humidity.dc-HIH6130.%s", rht.rh)
  time.sleep(60)

print "exiting"
